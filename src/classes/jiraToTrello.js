class JiraToTrello {
    constructor(jira, trello) {
        this.jira = jira;
        this.trello = trello;
    }

    async createTrelloCard(cardKeyJira, boardIdTrello) {
        const [cardData, boardList] = await Promise.all(
            [this.jira.getCardByName(cardKeyJira), this.trello.getBoardList(boardIdTrello)]);

        const { attachment, comment, description, duedate, summary } = cardData;

        const trelloCardDescription = this.dataMapper(description.content);

        const params = {
            idList: boardList.id,
            name: summary,
            desc: trelloCardDescription,
            due: duedate
        };

        const createdCard = await this.trello.createCard(params);

        if(attachment.length > 0) {
            await Promise.all(attachment.map(({ content, mimeType, filename }) => {
                return this.trello.addAttachmentToCard(createdCard.id, content, mimeType, filename);
            }));
        }

        if(comment.total > 0) {
            await Promise.all(comment.comments.map(comment => {
                const commentText = this.dataMapper(comment.body.content);
                return this.trello.addCommentToCard(createdCard.id, commentText);
            }));
        }

        return createdCard;
    }

    dataMapper(value) {
        if (!Array.isArray(value)) {
            return;
        }

        if (value.length === 0){
            return '';
        }

        let result = '';

        value.forEach(({ type, attrs, content }) => {
            if(type === 'heading') {
                if(attrs.level) {
                    content.forEach(({ type, text }) => {
                        if(type === 'text'){
                            result += `${'#'.repeat(attrs.level)}${text}\n\n`;
                        }
                    });
                }
            }

            if(type === 'paragraph') {
                if(content.length === 0){
                    result += '\n\n';
                }

                content.forEach(({ type, text, marks, attrs }, index) => {
                    if(type === 'text'){
                        if(!marks){
                            result += `${text}`;
                        }

                        if(marks){
                            marks.forEach(mark => {
                                if(mark.type === 'em'){
                                    result += `*${text}*`;
                                }
                                if(mark.type === 'strong'){
                                    result += `**${text}**`;
                                }
                                if(mark.type === 'strike'){
                                    result += `~~${text}~~`;
                                }
                                if(mark.type === 'code'){
                                    result += '`'+text+'`';
                                }
                                if(mark.type === 'link'){
                                    result += `[${text}](${mark.attrs.href})`;
                                }
                            })
                        }

                        if(index === content.length - 1){
                            result += '\n\n';
                        }
                    }
                    if(type === 'inlineCard'){
                        result += `${attrs.url}`;
                    }
                });
            }

            if(type === 'bulletList' || type === 'orderedList'){
                result += '\n\n';
                content.forEach(list => {
                    list.content.forEach(listElement => {
                        if(listElement.type === 'paragraph') {
                            listElement.content.forEach((paragraph, number) => {
                                if(paragraph.type === 'text'){

                                    if(type === 'bulletList') {
                                        result += `- ${paragraph.text}`;
                                    }

                                    if(type === 'orderedList') {
                                        result += `${number + 1}. ${paragraph.text}`;
                                    }

                                    if(number === listElement.content.length - 1){
                                        result += '\n\n';
                                    }
                                }
                                if(paragraph.type === 'inlineCard'){
                                    result += `${paragraph.attrs.url}`;
                                }
                            });
                        }
                    });
                });
            }
        });

        return result;
    }
}

exports.JiraToTrello = JiraToTrello;