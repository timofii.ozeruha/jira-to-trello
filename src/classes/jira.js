const axios = require("axios");

class Jira {
    #request = null;

    constructor(url, token, login) {
        this.#request = axios.create({
            baseURL: url,
            auth: {
                username: login,
                password: token
            }
        });
    }

    async getCardByName(cardName) {
        try {
            const response = await this.#request.get(`/rest/api/3/issue/${cardName}`);

            const { attachment, comment, description, duedate, summary } = response.data.fields;

            const cardData = {
                attachment,
                comment,
                description,
                duedate,
                summary
            };

            return cardData;

        } catch (error) {
            console.error(error);
        }
    }
}

exports.Jira = Jira;