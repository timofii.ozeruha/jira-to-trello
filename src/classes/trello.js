const axios = require("axios");

class Trello {
    #request = null;

    constructor(url, authParams) {
        this.#request = axios.create({
            baseURL: url,
            params: authParams
        });
    }

    async getBoardList(boardId) {
        try {
            const response = await this.#request.get(`/1/boards/${boardId}/lists`);

            return response.data[0];

        } catch (error) {
            console.error(error);
        }
    }

    async createCard(params) {
        try {
            const response = await this.#request.post(`/1/cards`, null, { params });

            return response.data;

        } catch (error) {
            console.error(error);
        }
    }

    async addAttachmentToCard(cardId, ContentUrl, mimeType, filename) {
        try {
            const params = new URLSearchParams({
                url: ContentUrl,
                mimeType,
                name: filename,
            });

            await this.#request.post(`/1/cards/${cardId}/attachments`,
                params,
                { 'Content-Type': 'application/x-www-form-urlencoded' }
            );

        } catch (error) {
            console.error(error);
        }
    }

    async addCommentToCard(cardId, comment) {
        try {
            await this.#request.post(`/1/cards/${cardId}/actions/comments`,
                null,
                { params: {
                    text: comment
                }
            });

        } catch (error) {
            console.error(error);
        }
    }
}

exports.Trello = Trello;