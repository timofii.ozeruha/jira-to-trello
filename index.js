const { Jira } = require('./src/classes/jira.js');
const { Trello } = require('./src/classes/trello.js');
const { JiraToTrello } = require('./src/classes/jiraToTrello.js');

/* Jira */
const urlJira = 'https://timofiiozeruga.atlassian.net';
const apiTokenJira = 'sApDm5EOshiQRIVwDLPcC23A';
const loginJira = 'timofii.ozeruha@freshcodeit.com';
const cardKeyJira = 'TIM-1';

/* Trello */
const urlTrello = 'https://api.trello.com';
const authParamsTrello = {
    key: '8c870b555907ce04550832cebb5e308f',
    token: 'd9abb47f6c641185e485dcbd2d5fbcdd519d1d3087d1c6b09cea6ec236ff16f5',
};
const boardIdTrello = '635bc7810da29700a7a917f2';

const jira = new Jira(urlJira, apiTokenJira, loginJira);
const trello = new Trello(urlTrello, authParamsTrello);

const jiraToTrello = new JiraToTrello(jira, trello);
jiraToTrello.createTrelloCard(cardKeyJira, boardIdTrello);
